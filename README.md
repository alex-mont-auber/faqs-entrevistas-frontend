# FAQs entrevistas Frontend



# Links de interés


- "Cracking the front-end interview" -> Muy interesante, **repaso de conceptos** que pueden ser preguntados en una entrevista:

https://www.freecodecamp.org/news/cracking-the-front-end-interview-9a34cd46237/



- "Never Fail Tech Interview" -> Preguntas concretas Angular y frontend para preparar a fondo una entrevista.

https://www.fullstack.cafe/blog/front-end-developer-interview-questions



- "Front-end-Developer-Interview-Questions" -> Repo github con listado enorme de preguntas frontend.

https://github.com/h5bp/Front-end-Developer-Interview-Questions



**Buscar en google**:

frontend technical interview

frontend technical interview examples 

frontend technical interview questions

